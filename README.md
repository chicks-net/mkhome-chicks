Role Name
=========

Create `chicks` home directory.

Requirements
------------

Just ansible at this time.

Role Variables
--------------

A description of the settable variables for this role should go here, including any variables that are in defaults/main.yml, vars/main.yml, and any variables that can/should be set via parameters to the role. Any variables that are read from other roles and/or the global scope (ie. hostvars, group vars, etc.) should be mentioned here as well.

Dependencies
------------

No dependencies at this time.

Example Playbook
----------------

It might work like this:

    - hosts: servers
      roles:
         - fini.mkhome-chicks

License
-------

GPLv2

Authors
-------

* [Christopher Hicks](http://www.chicks.net)
